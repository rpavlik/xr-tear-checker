// Copyright (c) 2019-2022, The Khronos Group Inc.
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "util.h"
#include <stdexcept>
#include <string>

#include <openxr/openxr.h>

#ifdef XR_USE_PLATFORM_WIN32
#include "windows.h"
#endif

#ifdef XR_USE_PLATFORM_ANDROID
#include <android/log.h>
#include <stdlib.h>
#endif // XR_USE_PLATFORM_ANDROID

#include <fmt/format.h>

[[noreturn]] inline void Throw(std::string failureMessage, const char *originator = nullptr,
                               const char *sourceLocation = nullptr) {
    if (originator != nullptr) {
        failureMessage += fmt::format("\n    Origin: {}", originator);
    }

    if (sourceLocation != nullptr) {
        failureMessage += fmt::format("\n    Source: {}", sourceLocation);
    }
#ifdef XR_USE_PLATFORM_ANDROID
    // write to the log too
    __android_log_write(ANDROID_LOG_ERROR, "OpenXR_Conformance_Throw", failureMessage.c_str());
#endif
    throw std::logic_error(failureMessage);
}

#define XRC_THROW(msg) Throw(msg, nullptr, XRC_FILE_AND_LINE);

#define XRC_CHECK_THROW(exp)                                                                                           \
    {                                                                                                                  \
        if (!(exp)) {                                                                                                  \
            Throw("Check failed", #exp, XRC_FILE_AND_LINE);                                                            \
        }                                                                                                              \
    }

#define XRC_CHECK_THROW_MSG(exp, msg)                                                                                  \
    {                                                                                                                  \
        if (!(exp)) {                                                                                                  \
            Throw(msg, #exp, XRC_FILE_AND_LINE);                                                                       \
        }                                                                                                              \
    }

[[noreturn]] inline void ThrowXrResult(XrResult res, const char *originator = nullptr,
                                       const char *sourceLocation = nullptr) noexcept(false) {
    Throw(fmt::format("XrResult failure [{}]", int64_t(res)), originator, sourceLocation);
}

inline XrResult CheckThrowXrResult(XrResult res, const char *originator = nullptr,
                                   const char *sourceLocation = nullptr) noexcept(false) {
    if (XR_FAILED(res)) {
        ThrowXrResult(res, originator, sourceLocation);
    }

    return res;
}

inline XrResult CheckThrowXrResultUnqualifiedSuccess(XrResult res, const char *originator = nullptr,
                                                     const char *sourceLocation = nullptr) noexcept(false) {
    if (!XR_UNQUALIFIED_SUCCESS(res)) {
        ThrowXrResult(res, originator, sourceLocation);
    }

    return res;
}

#define XRC_THROW_XRRESULT(xr, cmd) ThrowXrResult(xr, #cmd, XRC_FILE_AND_LINE);
#define XRC_CHECK_THROW_XRCMD(cmd) CheckThrowXrResult(cmd, #cmd, XRC_FILE_AND_LINE);
#define XRC_CHECK_THROW_XRCMD_UNQUALIFIED_SUCCESS(cmd)                                                                 \
    CheckThrowXrResultUnqualifiedSuccess(cmd, #cmd, XRC_FILE_AND_LINE);
#define XRC_CHECK_THROW_XRRESULT(res, cmdStr) CheckThrowXrResult(res, cmdStr, XRC_FILE_AND_LINE);
#define XRC_CHECK_THROW_XRRESULT_SUCCESS_OR_LIMIT_REACHED(res, cmdStr)                                                 \
    CheckThrowXrResultSuccessOrLimitReached(res, cmdStr, XRC_FILE_AND_LINE);

#if defined(_WIN32) || defined(XRC_DOXYGEN)

[[noreturn]] inline void ThrowHResult(HRESULT hr, const char *originator = nullptr,
                                      const char *sourceLocation = nullptr) noexcept(false) {
    Throw(fmt::format("HRESULT failure [{:x}]", hr), originator, sourceLocation);
}

inline HRESULT CheckThrowHResult(HRESULT hr, const char *originator = nullptr,
                                 const char *sourceLocation = nullptr) noexcept(false) {
    if (FAILED(hr)) {
        ThrowHResult(hr, originator, sourceLocation);
    }

    return hr;
}

#define XRC_THROW_HR(hr, cmd) ThrowHResult(hr, #cmd, XRC_FILE_AND_LINE);
#define XRC_CHECK_THROW_HRCMD(cmd) CheckThrowHResult(cmd, #cmd, XRC_FILE_AND_LINE);
#define XRC_CHECK_THROW_HRESULT(res, cmdStr) CheckThrowHResult(res, cmdStr, XRC_FILE_AND_LINE);

#endif // defined(_WIN32) || defined(XRC_DOXYGEN)

/// @}
