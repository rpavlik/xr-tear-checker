// Copyright (c) 2019-2022, The Khronos Group Inc.
// Copyright (c) 2019 Collabora, Ltd.
//
// SPDX-License-Identifier: Apache-2.0
//
#pragma once

#include <openxr/openxr.h>

#include <Windows.h>
#include <stdexcept>
#include <string>

/// Implements an auto-initializing XrPosef via C++ construction.
/// @ingroup cts_framework
struct XrPosefCPP : public XrPosef {
    XrPosefCPP() : XrPosef{{0, 0, 0, 1}, {0, 0, 0}} {}
};

#define XRC_CHECK_STRINGIFY(x) #x
#define XRC_TO_STRING(x) XRC_CHECK_STRINGIFY(x)

/// Represents a compile time file and line location as a single string.
#define XRC_FILE_AND_LINE __FILE__ ":" XRC_TO_STRING(__LINE__)

/// Returns a pointer to an extension function retrieved via xrGetInstanceProcAddr.
///
/// Example usage:
/// ```
///     XrInstance instance; // ... a valid instance
///     auto _xrPollEvent = GetInstanceExtensionFunction<PFN_xrPollEvent>(instance, "xrPollEvent");
///     CHECK(_xrPollEvent != nullptr);
/// ```
///
template <typename FunctionType, bool requireSuccess = true>
FunctionType GetInstanceExtensionFunction(XrInstance instance, const char *functionName) {
    if (instance == XR_NULL_HANDLE) {
        throw std::logic_error("Cannot pass a null instance to GetInstanceExtensionFunction");
    }
    if (functionName == nullptr) {
        throw std::logic_error("Cannot pass a null function name to GetInstanceExtensionFunction");
    }
    FunctionType f = nullptr;
    XrResult result = xrGetInstanceProcAddr(instance, functionName, (PFN_xrVoidFunction *)&f);
    if (requireSuccess) {
        if (result != XR_SUCCESS) {
            throw std::runtime_error(fmt::format("Failed trying to get function {}: {:x}", functionName, (int64_t)result));
        }
    }

    if (XR_SUCCEEDED(result)) {
        if (f == nullptr) {
            throw std::runtime_error(
                std::string("xrGetInstanceProcAddr claimed to succeed, but returned null trying to get function ") +
                functionName);
        }
    }

    return f;
}

/// Example usage:
/// ```
/// XrDuration timeout = 10_xrSeconds;
/// ```
inline constexpr XrDuration operator"" _xrSeconds(unsigned long long value) {
    return (static_cast<int64_t>(value) * 1000 * 1000 * 1000); // Convert seconds to XrDuration nanoseconds.
}

/// Example usage:
/// ```
/// XrDuration timeout = 10_xrMilliseconds;
/// ```
inline constexpr XrDuration operator"" _xrMilliseconds(unsigned long long value) {
    return (static_cast<int64_t>(value) * 1000 * 1000); // Convert milliseconds to XrDuration nanoseconds.
}

/// Example usage:
/// ```
/// XrDuration timeout = 10_xrMicroseconds;
/// ```
inline constexpr XrDuration operator"" _xrMicroseconds(unsigned long long value) {
    return (static_cast<int64_t>(value) * 1000); // Convert microseconds to XrDuration nanoseconds.
}

/// Example usage:
/// ```
/// XrDuration timeout = 10_xrNanoseconds;
/// ```
inline constexpr XrDuration operator"" _xrNanoseconds(unsigned long long value) {
    return static_cast<int64_t>(value); // XrDuration is already in nanoseconds
}
