// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <openxr/openxr.h>

#ifdef XR_USE_TIMESPEC
#include <time.h>
#endif

#ifdef XR_USE_PLATFORM_WIN32

#ifndef NOMINMAX
#define NOMINMAX
#endif  // !NOMINMAX

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif  // !WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <Unknwn.h>

#endif  // XR_USE_PLATFORM_WIN32

#ifdef XR_USE_GRAPHICS_API_D3D11
#include <d3d11.h>
#endif  // XR_USE_GRAPHICS_API_D3D11

#ifdef XR_USE_GRAPHICS_API_VULKAN
#include <vulkan/vulkan.h>
#endif  // XR_USE_GRAPHICS_API_VULKAN

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
typedef void* Display;
typedef void* GLXFBConfig;
typedef void* GLXDrawable;
typedef void* GLXContext;
#endif

#include <openxr/openxr_platform.h>
