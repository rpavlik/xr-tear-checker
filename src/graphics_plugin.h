// Copyright (c) 2019-2022, The Khronos Group Inc.
//
// SPDX-License-Identifier: Apache-2.0
//

#pragma once

#include "nonstd/span.hpp"
#include <cstdint>
#include <functional>
#include <memory>
#include <openxr/openxr.h>
#include <stdexcept>
#include <string>
#include <vector>

// We #include all the possible graphics system headers here because openxr_platform.h assumes that
// they are all visible when it is compiled.

#if defined(XR_USE_GRAPHICS_API_VULKAN)
#ifdef XR_USE_PLATFORM_WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#endif
#ifdef XR_USE_PLATFORM_ANDROID
#define VK_USE_PLATFORM_ANDROID_KHR
#endif
#include <vulkan/vulkan.h>
#endif

#if defined(XR_USE_GRAPHICS_API_OPENGL)

#if defined(_WIN32)
#include <windows.h>
#endif // defined(_WIN32)

#if defined(__APPLE__)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif // defined(__APPLE__)

#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#endif

#ifdef XR_USE_GRAPHICS_API_D3D11
#include <d3d11_4.h>
#endif

#ifdef XR_USE_GRAPHICS_API_D3D12
#include <d3d12.h>
#endif

class ISwapchainImageData;

/// Color constant used as the default clear color.
constexpr XrColor4f DarkSlateGrey = {0.184313729f, 0.309803933f, 0.309803933f, 1.0f};

/// Wraps a graphics API so the main openxr program can be graphics API-independent.
struct IGraphicsPlugin {
    virtual ~IGraphicsPlugin() = default;

    /// Required before use of any member functions as described for each function.
    virtual bool Initialize() = 0;

    /// Identifies if the IGraphicsPlugin has successfully initialized.
    /// May be called regardless of initialization state.
    virtual bool IsInitialized() const = 0;

    /// Matches Initialize.
    /// May be called only if successfully initialized.
    virtual void Shutdown() = 0;

    /// Returns a string describing the platform.
    /// May be called regardless of initialization state.
    /// Example returned string: "OpenGL"
    virtual std::string DescribeGraphics() const = 0;

    /// OpenXR extensions required by this graphics API.
    virtual std::vector<std::string> GetInstanceExtensions() const = 0;

    /// Create an instance of this graphics api for the provided XrInstance and XrSystemId.
    /// If checkGraphicsRequirements is false then InitializeDevice intentionally doesn't call
    /// xrGetxxxxGraphicsRequirementsKHR before initializing a device.
    virtual bool InitializeDevice(XrInstance instance, XrSystemId systemId, bool checkGraphicsRequirements = true,
                                  uint32_t deviceCreationFlags = 0) = 0;

    /// Clear any memory associated with swapchains, particularly auto-created accompanying depth buffers.
    virtual void ClearSwapchainCache() = 0;

    /// Some graphics devices can accumulate memory usage unless you flush them, and some of our
    /// tests create and destroy large amounts of memory.
    virtual void Flush() {
        // Default no-op implementation for APIs which don't need flushing.
    }

    /// Call to check the validity of the graphics state (useful when checking for interactions with OpenXR calls).
    virtual void CheckState(const char * /*file_line*/) const {
        // Default no-op implementation for APIs which don't need checking.
    }

    /// Called when changing graphics interaction thread.
    virtual void MakeCurrent(bool /*bindToThread*/) {
        // Default no-op implementation for APIs which don't need binding.
    }

    /// Shuts down the device initialized by InitializeDevice. Restores to the same state as prior to
    /// the call to InitializeDevice.
    virtual void ShutdownDevice() = 0;

    /// Get the graphics binding header for session creation.
    /// Must have successfully called InitializeDevice before calling this or else this returns nullptr.
    virtual const XrBaseInStructure *GetGraphicsBinding() const = 0;

    /// Select the preferred swapchain format.
    virtual int64_t GetSRGBA8Format() const = 0;

    /// Allocates an object owning (among other things) an array of XrSwapchainImage* in a portable way and
    /// returns an **observing** pointer to an interface providing generic access to the associated pointers.
    /// (The object remains owned by the graphics plugin, and will be destroyed on @ref ShutdownDevice())
    /// This is all for the purpose of being able to call the xrEnumerateSwapchainImages function
    /// in a platform-independent way. The user of this must not use the images beyond @ref ShutdownDevice()
    ///
    /// Example usage:
    ///
    /// ```c++
    /// ISwapchainImageData * p = graphicsPlugin->AllocateSwapchainImageData(3, swapchainCreateInfo);
    /// xrEnumerateSwapchainImages(swapchain, 3, &count, p->GetColorImageArray());
    /// ```
    virtual ISwapchainImageData *AllocateSwapchainImageData(size_t size,
                                                            const XrSwapchainCreateInfo &swapchainCreateInfo) = 0;

    virtual void ClearImageSlice(const XrSwapchainImageBaseHeader *colorSwapchainImage, uint32_t imageArrayIndex = 0,
                                 XrColor4f bgColor = DarkSlateGrey) = 0;
};

/// Create a graphics plugin for the graphics API specified in the options.
/// Throws std::invalid_argument if the graphics API is empty, unknown, or unsupported.
std::shared_ptr<IGraphicsPlugin> CreateGraphicsPlugin(const char *graphicsAPI) noexcept(false);

std::shared_ptr<IGraphicsPlugin> CreateGraphicsPlugin_D3D11();
