# Copyright 2019-2023, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

cmake_minimum_required(VERSION 3.9.0)
project(OpenXRPrintTracker)

set(CMAKE_CXX_STANDARD 14)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(GNUInstallDirs)
include(EnableExtraCompilerWarnings)
globally_enable_extra_compiler_warnings()

find_package(OpenXR REQUIRED COMPONENTS loader)

find_package(Threads)

set(BUILD_WITH_D3D11 ON)
# option(BUILD_WITH_D3D11 "Build with D3D11 backend?" ON)
# option(BUILD_WITH_VULKAN "Build with Vulkan backend?" OFF)
# option(BUILD_WITH_OPENGL_XLIB "Build with OpenGL backend on xlib?" OFF)

add_library(openxr-hpp INTERFACE)
target_include_directories(openxr-hpp INTERFACE vendor/openxr-hpp)
add_subdirectory(vendor/fmt-9.1.0)
add_subdirectory(src)


